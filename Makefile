
# This Makefile helps the build of the SO3 environment for SYE labs

.PHONY: so3 libc usr compile deploy clean

all:	compile deploy
	echo "Compilation done"

compile: libc usr so3
	echo "Compilation done"

deploy:
	echo "Deploying..."
	./deploy.sh -br
	

init:
	cd filesystem && ./create_img.sh vexpress
	cd rootfs && ./create_ramfs.sh vexpress && ./deploy.sh vexpress

so3:
	cd so3 && make

libc:
	cd usr/libc/ && make

usr:
	@echo Compiling usr and deploying in the root filesystem ...
	cd usr && make 


clean:
	cd so3 && make clean
	cd usr/libc && make clean
	cd usr && make clean
